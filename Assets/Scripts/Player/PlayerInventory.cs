﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Player
{
    class PlayerInventory : MonoBehaviour
    {

        [SerializeField] private Weapon weapon;
        [SerializeField] private Armor[] armor = new Armor[4];

        public Weapon GetWeapon()
        {
            return weapon;
        }

        public void SetWeapon(Weapon newWeapon)
        {
            this.weapon = newWeapon;
        }

        public Armor[] GetArmor()
        {
            return armor;
        }

        public void SetArmor(Armor[] newArmor)
        {
            armor = newArmor;
        }

        public void putArmor(Armor armorPiece)
        {
            switch(armorPiece.armorType)
            {
                case Armor.ArmorType.HELMET:
                    armor[0] = armorPiece;
                    break;
                case Armor.ArmorType.CHESTPLATE:
                    armor[1] = armorPiece;
                    break;
                case Armor.ArmorType.LEGGINGS:
                    armor[2] = armorPiece;
                    break;
                case Armor.ArmorType.BOOTS:
                    armor[3] = armorPiece;
                    break;
            }
        }

    }
}
