﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="Armor", menuName ="Custom Stuff/Armor", order=0)]
public class Armor : ScriptableObject
{

    public enum ArmorType
    {
        HELMET, CHESTPLATE, LEGGINGS, BOOTS
    }

    public int attackDamage;
    public float attackSpeed;
    public int prospection;
    public int luck;
    public float critical;
    public Sprite sprite;
    public ArmorType armorType;

}
