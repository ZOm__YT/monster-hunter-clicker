﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Weapon", menuName = "Custom Stuff/Weapon", order = 1)]
public class Weapon : ScriptableObject
{

    public int attackDamage;
    public float attackSpeed;
    public Sprite sprite;

}
