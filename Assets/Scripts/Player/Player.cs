﻿using Assets.Scripts.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    private float attackSpeed;

    private SpriteRenderer spriteRenderer;
    private float timer;

    private PlayerStats stats = new PlayerStats();
    private PlayerInventory inventory;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        stats.attackDamage = 1;
        stats.attackSpeed = 2f;
    }

    void Start()
    {
        inventory = GameObject.Find("PlayerInventory").GetComponent<PlayerInventory>();
    }

    // Update is called once per frame
    void Update()
    {
        attackSpeed = stats.attackSpeed + inventory.GetWeapon().attackSpeed;
        if (timer < attackSpeed)
        {
            timer += Time.deltaTime;
        }
        else if (timer >= attackSpeed)
        {
            timer = 0;
            Attack();
        }
    }

    void Attack()
    {
        Enemy enemy = GameObject.FindGameObjectWithTag("Enemy").GetComponent<Enemy>();
        float atk = stats.attackDamage + inventory.GetWeapon().attackDamage;
        Debug.Log("Enemy attacked!");
        enemy.lifeState -= atk;
        if (enemy.lifeState <= 0)
        {
            enemy.lifeState = 10;
            Debug.Log("Enemy Die !");
        }
    }

}
