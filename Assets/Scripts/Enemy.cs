﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    public float lifeState = 50;
    public int xpValue;
    public LootTable[] lootTable;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.touchCount > 0)
        {

            Debug.Log("njUSI");

        }

        if (lifeState == 0)
        {

            onDeath();

        }

    }

    void onDeath()
    {

        for(int i = 0; i < lootTable.Length; i++)
        {

            float rand = Random.Range(0, 1f);

            if(rand <= (lootTable[i].chanceDrop/100))
            {

                Debug.Log(lootTable[i].name);
                GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>().goldWalet += lootTable[i].goldValue;

            }

        }

        Debug.Log("Enemy death !!");

        Destroy(gameObject);

    }


    [System.Serializable]
    public class LootTable
    {
        public string name;
        [Range(0, 100)]
        public float chanceDrop;
        public int goldValue;
        public Sprite sprite;

    }

}
