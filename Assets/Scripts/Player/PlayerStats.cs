﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class PlayerStats
{

    public int attackDamage;
    public float attackSpeed;
    public int prospection;
    public int luck;
    public float critical;

}
